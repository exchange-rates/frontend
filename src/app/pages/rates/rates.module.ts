import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatesComponent } from './rates.component';
import { RatesRoutingModule } from './rates-routing.module';
import { BankRatesCardModule } from '../../components/bank-rates-card/bank-rates-card.module';
import { ApiModule } from '../../modules/api/api.module';
import { LocalStorageModule } from '../../modules/local-storage/local-storage.module';

@NgModule({
  declarations: [ RatesComponent ],
  imports: [
    CommonModule,
    RatesRoutingModule,
    BankRatesCardModule,
    ApiModule,
    LocalStorageModule
  ]
})
export class RatesModule {}
