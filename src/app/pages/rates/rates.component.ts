import { Component, OnDestroy, OnInit } from '@angular/core';
import { CurrenciesEnum } from '../../utils/currencies.enum';
import { ApiService } from '../../modules/api/api.service';
import { Subscription } from 'rxjs';
import { LocalStorageService } from '../../modules/local-storage/local-storage.service';
import { BankRate } from '../../utils/BankRate';

interface CurrenciesMenuItem {
  value: CurrenciesEnum;
  title: string;
}

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: [ './rates.component.scss' ]
})
export class RatesComponent implements OnInit, OnDestroy {

  rates: BankRate[] = [];
  $sub: Subscription;
  currentCurrency: CurrenciesEnum;
  currencies: CurrenciesMenuItem[] = [
    { value: CurrenciesEnum.USD, title: ' $ USD' },
    { value: CurrenciesEnum.EUR, title: ' € EUR' },
    { value: CurrenciesEnum.RUB, title: ' ₽ RUB' },
  ];

  constructor(
    private api: ApiService,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    this.currentCurrency = this.localStorageService.getCurrency();
    this.getRatesByCurrency(this.currentCurrency);
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  getRatesByCurrency(currency: CurrenciesEnum) {
    this.unsubscribe();

    this.$sub = this.api.getRatesByCurrency(currency)
      .subscribe(rates => this.rates = rates);
  }

  changeCurrency(currency: CurrenciesEnum) {
    if (currency !== this.currentCurrency) {
      this.currentCurrency = currency;
      this.localStorageService.setCurrency(currency);
      this.getRatesByCurrency(currency);
    }
  }

  unsubscribe() {
    if (this.$sub) { this.$sub.unsubscribe(); }
  }
}
