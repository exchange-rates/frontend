import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-wrapper-with-footer>
      <router-outlet></router-outlet>
    </app-wrapper-with-footer>
  `
})
export class AppComponent {}
