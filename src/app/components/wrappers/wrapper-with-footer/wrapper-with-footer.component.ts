import { Component } from '@angular/core';

@Component({
  selector: 'app-wrapper-with-footer',
  templateUrl: './wrapper-with-footer.component.html',
  styleUrls: [ './wrapper-with-footer.component.scss' ]
})
export class WrapperWithFooterComponent {}
