import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WrapperWithFooterComponent } from './wrapper-with-footer/wrapper-with-footer.component';
import { FooterModule } from '../footer/footer.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ WrapperWithFooterComponent ],
  imports: [
    CommonModule,
    FooterModule,
    RouterModule
  ],
  exports: [ WrapperWithFooterComponent ]
})
export class WrappersModule {}
