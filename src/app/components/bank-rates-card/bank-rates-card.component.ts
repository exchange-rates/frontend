import { Component, Input } from '@angular/core';
import { BankRate } from '../../utils/BankRate';
import { BanksEnum } from '../../utils/banks.enum';

@Component({
  selector: 'app-bank-rates-card',
  templateUrl: './bank-rates-card.component.html',
  styleUrls: [ './bank-rates-card.component.scss' ]
})
export class BankRatesCardComponent {

  @Input()
  bankRates: BankRate;

  getImgSrc(rate: BanksEnum): string {
    return `assets/img/${rate.toLowerCase()}.png`;
  }
}
