import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankRatesCardComponent } from './bank-rates-card.component';
import { PipesModule } from '../../modules/pipes/pipes.module';

@NgModule({
  declarations: [ BankRatesCardComponent ],
  exports: [ BankRatesCardComponent ],
  imports: [
    CommonModule,
    PipesModule
  ]
})
export class BankRatesCardModule {}
