import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { CurrenciesEnum } from '../../utils/currencies.enum';
import { BankRate } from '../../utils/BankRate';

@Injectable()
export class ApiService {

  collection: AngularFirestoreCollection<BankRate>;

  constructor(
    private db: AngularFireDatabase,
    private firestore: AngularFirestore
  ) {}

  getRatesByCurrency(currency: CurrenciesEnum): Observable<BankRate[]> {
    this.collection = this.firestore.collection(currency);
    return this.collection.valueChanges();
  }
}
