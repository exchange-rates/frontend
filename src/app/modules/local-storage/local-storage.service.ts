import { Injectable } from '@angular/core';
import { CurrenciesEnum } from '../../utils/currencies.enum';

@Injectable()
export class LocalStorageService {

  currency: CurrenciesEnum;

  setItem<T>(key: string, item: T): void {
    localStorage.setItem(key, JSON.stringify(item));
  }

  getItem<T>(key: string): T {
    const data: any = localStorage.getItem(key);

    if (!data) { return null; }

    let obj: T;

    try {
      obj = (JSON.parse(data) as T);
    } catch (error) {
      obj = null;
    }

    return obj;
  }

  setCurrency(currency: CurrenciesEnum) {
    this.setItem('currency', currency);
  }

  getCurrency(): CurrenciesEnum {
    const currency = this.getItem<CurrenciesEnum>('currency');
    if (currency) {
      return currency;
    } else {
      this.setCurrency(CurrenciesEnum.USD);
      return CurrenciesEnum.USD;
    }
  }
}
