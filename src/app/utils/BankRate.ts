import { BanksEnum } from './banks.enum';

export class BankRate {
  bank: BanksEnum;
  buy: number;
  buy_diff: number;
  sale: number;
  sale_diff: number;
  date: Date;

  constructor(data, bank) {
    this.bank = bank;
    this.buy = data.buy;
    this.buy_diff = data.buy_diff;
    this.sale = data.sale;
    this.sale_diff = data.sale_diff;
    this.date = data.date.toDate();
  }
}
