export enum CurrenciesEnum {
  USD = 'USD',
  EUR = 'EUR',
  RUB = 'RUB',
  UAH = 'UAH'
}
